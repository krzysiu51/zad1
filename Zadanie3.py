#!/usr/bin/env python3


class Flower:
    def __init__(self, name, color):
        self.name = name
        self.color = color

    def describe_flower(self):
        print("The flower is called {} and it has {} color".format(self.name, self.color))


name = input("Enter flower name: ")
color = input("Enter flower color: ")

flower = Flower(name, color)
flower.describe_flower()


